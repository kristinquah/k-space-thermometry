function output=ift2(in)
% Performs fftshift(ifft(fftshift( input)))
% 1D inverse FT
output = ifftshift(ifft2(ifftshift(in)));
