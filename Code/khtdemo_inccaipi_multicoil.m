%| Demonstration of k-space temperature reconstruction algorithm from
%| two-slice SMS acquisition with incoherent CAIPI blipping
%|
%| Copyright 2018, William A Grissom, Megan Poorman, Kristin Quah, Vanderbilt University

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load the data and get the baselines
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath('util');
simulatedData = true; % REDFLAG false = untested!
load Mask3dxyz
load Loop32RxAll
nSlices = 3; % how many slices to simultaneously image
in = 1;
nLib = 1; % how many library entries to use, for testing multibaseline
inPlaneAcc = 1; % in-plane acceleration factor
noiseStd = 1/50; % standard deviation of noise added to heating data (~1/SNR) 1/50 is a good value
modelTest = false; % mode to measure temperature precision with no actual heating
if ~simulatedData
    load khtdemo_data_cart;
    samePhase = true;
    maxtind = 9; % 9th dynamic has highest temp
    dacc = permute(data_img(:,:,:,maxtind),[3 1 2]); % extract sampled lines for this dynamic
    dacc = dacc(:,:).';
    [Nx,Ny,Nc,Nt] = size(data_img); % # x,y-locs, coils, dynamics

    % recon the baselines & duplicate to other slices
    for ii = 1:Nc
        L(:,:,ii) = ift2(sqz(data_img(:,:,ii,1)))*Nx*Ny;
    end
    L = repmat(L,[1 1 1 nSlices]);

    % Duplicate data & baselines to other slices
    if samePhase
        dacc = repmat(dacc,[1 1 nSlices]);
        hotSpot = repmat(hotSpot,[1 1 nSlices]);
    else
        % copy baseline data to other slices
        dtmp = permute(data_img(:,:,:,1),[3 1 2]); % extract sampled lines for this dynamic
        dtmp = dtmp(:,:).';
        for ii = 2:nSlices
            dacc(:,:,ii) = dtmp;
        end
        hotSpot = cat(3,hotSpot,zeros([size(hotSpot) nSlices-1]));
    end
else
    N = 128;Nx = N;Ny = N;Nc = size(RxB1m3dxyz,4);
    yPos = [16 16 16 16 16]; % zeros(nSlices,1);
    sigma = [10 10 10 10 10];%20*ones(nSlices,1); % stdev of hot spots
    img = padarray(permute(Mask3dxyz(:,:,35,:),[1 2 4 3]),[14 14],0);
    img = img(1:end-1, 1:end-1)';
    sensMap = padarray(permute(RxB1m3dxyz(:,:,35,:),[1 2 4 3]), [14 14],0);
    sensMap = sensMap(1:end-1, 1:end-1,:);

    %img = repmat(img,[1 1 nSlices]);
    imgAll = repmat(img,[1 1 nSlices]);
    %sliceAmps = [0.9 1 1.1];
    %for ii = 1:nSlices
    %    imgAll(:,:,ii) = sliceAmps(ii)*img; % + img.*abs(randn(size(img)));
    %end
    img = imgAll; clear imgAll;
    [x,y] = meshgrid(-N/2:N/2-1);
    for ii = 1:nSlices
        hotSpot(:,:,ii) = double(~modelTest)*(-3*pi/4)*exp(-(x.^2 + (y-yPos(ii)).^2)./sigma(ii)^2); % 18 degree heat
        hotSpot(:,:,ii) = hotSpot(:,:,ii).*(img(:,:,ii) > 0); % to mask out temp outside volume for error calc
    end
    imgHot = img.*exp(1i*hotSpot);
    L = permute(img,[1 2 4 3])*Nx*Ny;
    if nLib > 1 % synthesize some library images at different positions, to test multibaseline
        for ii = 2:nLib
            L = cat(3,L,circshift(L,(ii-1)*round(Nx/nLib),2));
        end
    end

    if Nc == size(sensMap,3)
        L = repmat(L,[1 1 1 1 Nc]).*permute(repmat(sensMap,[1 1 1 nSlices nLib]),[1 2 5 4 3]); % baseline for each coil (Nx, Ny, nLib, nSlices, Nc)
        imgHot = repmat(imgHot,[1 1 1 Nc]).*permute(repmat(sensMap, [1 1 1 nSlices]),[1 2 4 3]); % Nx,Ny,nSlices,Nc

        sensComp = permute(sensMap,[3 1 2]); % #coils, x, y-locs
        sensComp = permute(sensComp(:,:),[1 2]); % collapse the spatial dim
        disp('perform coil compression at  5% tollerance.')
        D = reshape(sensComp,size(sensComp,2),size(sensComp,1));
        [U,S,V] = svd(D,'econ');
        Nc = max(find(diag(S)/S(1)>0.05));
        disp(sprintf('Using %d virtual channels',Nc'));
        D_img = reshape(imgHot,size(imgHot,1)*size(imgHot,2)*size(imgHot,3),size(imgHot,4));
        D_base = reshape(L,size(L,1)*size(L,2)*size(L,3)*size(L,4),size(L,5));
        imgHot = reshape(D_img*V(:,1:Nc),size(imgHot,1),size(imgHot,2),nSlices,Nc);
        L = reshape(D_base*V(:,1:Nc),size(L,1),size(L,2),nLib,nSlices,Nc);
        sensMap = reshape(D*V(:,1:Nc),size(sensMap,1)*size(sensMap,2),Nc);
        noise_multicoil = noiseStd*ft2(sensMap);
    end
    for ii = 1:nSlices
        for jj = 1:Nc
            dacc(:,jj,ii) = col(ft2(imgHot(:,:,ii,jj)));
        end
    end
end
ct = -7.7871; % degrees C/radian (phase->temp conversion factor)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the 'accelerated' data and sampled k-space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
k = false(Nx,1);
k(1:inPlaneAcc:end) = true;
dacc = dacc(repmat(k,[Nx 1]),:,:);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% define the phase shifts & apply to one slice's data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
signPatternType = 'rand'; % switch between 'convCAIPI', 'rand', 'chaotic' and 'RF'
switch signPatternType
    case 'chaotic'
        % get the chaotic sign pattern to modulate data with in k-space
        x = 0.5; % initial x
        r = 3.99; % growth rate
        for ii = 1:(nSlices-1)*Nx-1
            x(ii+1) = r*x(ii)*(1-x(ii));
        end
        kWts = [(-1).^(reshape(x(:),[Nx nSlices-1]) > 0.5) ones(Nx,1)];
    case 'convCAIPI'
        % generate phase ramps that shift the slices by fov/sliceIndex
        kWts = exp(1i*2*pi/N*(-N/2:N/2-1)'*(N/nSlices)*(0:nSlices-1));
    case 'rand' % gradient blips
        % generate phase ramps that shift the slices by fov/sliceIndex
        kWts = exp(1i*2*pi/N*(-N/2:N/2-1)'*(N/nSlices)*(0:nSlices-1));
        % randomly permute them
        load permutation
        %permutation = randperm(N);
        load perm % 50 diff permutations
        kWts = kWts(perm(in,:),:);
        if nSlices > 1
            kWts(:,2:end) = -kWts(:,2:end);
        end
    case 'RF'
        if nSlices == 1
            kWts = ones(N,1);
        elseif nSlices == 2
            % 2 unique encodings
            encode1 = [1 1j];
            encode2 = [1 -1j];
            % Distribute encodings to each line randomly
            load distribution.mat
            kWts1 = distribution(:,in);
            %kWts1 = rand(N,1);
            kWts(kWts1<=1/2,:) = repmat(encode1,[sum(kWts1<=1/2),1]);
            kWts(kWts1>1/2,:) = repmat(encode2,[sum(kWts1>1/2),1]);
        elseif nSlices == 3
            % 4 unique encodings
            encode1 = [1 1 1];
            encode2 = [1 1 -1];
            encode3 = [1 -1 1];
            encode4 = [1 -1 -1];
            % Distribute encodings to each line randomly
            load distribution.mat
            kWts1 = distribution(:,in);
            %kWts1 = rand(N,1);
            kWts(kWts1<=1/4,:) = repmat(encode1,[sum(kWts1<=1/4),1]);
            kWts(kWts1>1/4 & kWts1<=1/2,:) = repmat(encode2,[sum(kWts1>1/4 & kWts1<=1/2),1]);
            kWts(kWts1>1/2 & kWts1<=3/4,:) = repmat(encode3,[sum(kWts1>1/2 & kWts1<=3/4),1]);
            kWts(kWts1>3/4 & kWts1<=1,:) = repmat(encode4,[sum(kWts1>3/4 & kWts1<=1),1]);
        end
end
% duplicate to all freq enc points
kWts = kron(ones(Nx,1),kWts); % duplicate to all frequency encoded points
kWts = kWts(repmat(k,[Nx 1]),:); % remove unsampled lines
daccCaipi = dacc;%.*repmat(kWts,[1 Nc]);

for ii = 1:nSlices
     daccCaipi(:,:,ii) = bsxfun(@times,dacc(:,:,ii),kWts(:,ii));%.*repmat(kWts,[1 Nc]);
end

figure;
imagesc(abs(ft2(sum(sum(reshape(daccCaipi,[sum(k) Ny Nc nSlices]),4),3))));
axis image,colormap gray,axis off
figure;
imagesc(-angle(ft2(sum(sum(reshape(daccCaipi,[sum(k) Ny Nc nSlices]),4),3))),[-pi/2 pi/2]);
axis image;axis off;colorbar

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4x-accelerated k-space recon with FFT's
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
thetainit = zeros(Nx,Ny,nSlices); % initialize temp phase shift map with zeros
if Nc == 1
    acqp.data = sum(daccCaipi,3) + ...
        noiseStd*N/sqrt(2)*(randn(size(sum(daccCaipi,3))) + 1i*randn(size(sum(daccCaipi,3)))); % accelerated data
else
    acqp.data = sum(daccCaipi,3); + ...
        noise_multicoil*N/sqrt(2).*(randn(size(sum(daccCaipi,3))) + 1i*randn(size(sum(daccCaipi,3))));
end
acqp.k = k(:,1); % k-space sampling mask - passing a vector tells ksh to
                 % preemptively do FFT in fully-sampled dimension, and run in hybrid space
acqp.kWts = kWts; %(repmat(k,[Nx 1]),1:nSlices); % in case we don't use all the weights for testing
acqp.L = L; % baseline 'library'
acqp.L = permute(acqp.L,[3 4 1 2 5]); % permute to # library images, # slices, space, rx coil
acqp.L = permute(acqp.L(:,:,:),[3 1 2]); % collapse spatial dimensions and permute to space, # library images, # slices
%acqp.L = acqp.L(:,:).'; % slices is second dimension
algp.order = 0; % polynomial order
algp.lam = [10^-4 -1]; % sparsity regularization parameter
algp.beta = 2^-16; % roughness regularization parameter
algp.useGPU = false; % cast all variables to the GPU (Cartesian Only)
algp.modeltest = modelTest; % measure temperature errors without heating
tic
[thetakacc,~,~,~,Ac] = kspace_hybrid_thermo_inccaipi(acqp,thetainit,algp);
tempkacc = ct*real(thetakacc);
toc
rmsError = norm(ct*(thetakacc(:)-hotSpot(:)))/N;
maxError = max(abs(ct*(thetakacc(:)-hotSpot(:))));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Show the results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
if modelTest
    maxDisplayTemp = 1;minDisplayTemp = -1;
else
    maxDisplayTemp = max(ct*hotSpot(:));minDisplayTemp = 0;
end
for ii = 1:nSlices
    subplot(3,nSlices,ii); imagesc(ct*hotSpot(:,:,ii),[minDisplayTemp maxDisplayTemp]); axis image;axis off
    h = colorbar; ylabel(h,'degrees C');
    title(sprintf('Truth, slice %d',ii));
    subplot(3,nSlices,nSlices+ii); imagesc(tempkacc(:,:,ii),[minDisplayTemp maxDisplayTemp]); axis image;axis off
    h = colorbar; ylabel(h,'degrees C');
    title(sprintf('Incoherent CAIPI, slice %d',ii));
    subplot(3,nSlices,2*nSlices+ii); imagesc(abs(ct*hotSpot(:,:,ii)-tempkacc(:,:,ii)),[minDisplayTemp 1]); axis image;axis off
    h = colorbar; ylabel(h,'degrees C');
    title(sprintf('Error, slice %d',ii));
end

% temp = ct*thetakacc.*(img>0);
% temp(img==0) = NaN;
% stdValue(nSlices) = std(temp(:),'omitnan')
