# README #

This repository contains examples and code for k-space hybrid thermometry, described in [Gaur et al, 2015](http://dx.doi.org/10.1002/mrm.25327). This code requires that Jeff Fessler's [Image Reconstruction Toolbox](http://web.eecs.umich.edu/~fessler/code/index.html) is installed.

Please see [wgrissom.com](http://wgrissom.com) for other software and info about our work.