%| Demonstration of k-space temperature reconstruction algorithm from
%| two-slice SMS acquisition with incoherent CAIPI blipping
%|
%| Copyright 2018, William A Grissom, Megan Poorman, Kristin Quah, Vanderbilt University

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load the data and get the baselines
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
simulatedData = true; % REDFLAG false = untested!
nSlices = 2; % how many slices to simultaneously image
nLib = 1; % how many library entries to use, for testing multibaseline
inPlaneAcc = 1; % in-plane acceleration factor
noiseStd = 1/50; % standard deviation of noise added to heating data (~1/SNR) 1/50 is a good value
modelTest = false; % mode to measure temperature precision with no actual heating
if ~simulatedData
    load khtdemo_data_cart;
    samePhase = true;
    maxtind = 9; % 9th dynamic has highest temp
    dacc = permute(data(:,:,:,maxtind),[3 1 2]); % extract sampled lines for this dynamic
    dacc = dacc(:,:).';
    [Nx,Ny,Nc,Nt] = size(data); % # x,y-locs, coils, dynamics

    % recon the baselines & duplicate to other slices
    for ii = 1:Nc
        L(:,:,ii) = fftshift(ifft2(fftshift(sqz(data(:,:,ii,1)))))*Nx*Ny;
    end
    L = repmat(L,[1 1 1 nSlices]);

    % Duplicate data & baselines to other slices
    if samePhase
        dacc = repmat(dacc,[1 1 nSlices]);
        hotSpot = repmat(hotSpot,[1 1 nSlices]);
    else
        % copy baseline data to other slices
        dtmp = permute(data(:,:,:,1),[3 1 2]); % extract sampled lines for this dynamic
        dtmp = dtmp(:,:).';
        for ii = 2:nSlices
            dacc(:,:,ii) = dtmp;
        end
        hotSpot = cat(3,hotSpot,zeros([size(hotSpot) nSlices-1]));
    end
else
    N = 128;Nx = N;Ny = N;Nc = 1;
    yPos = [32 0 -16 16 -8]; % zeros(nSlices,1);
    img = abs(phantom(N,[1 0.72 0.95 0 0 0]));
    %img = repmat(img,[1 1 nSlices]);
    imgAll = repmat(img,[1 1 nSlices]);
    %sliceAmps = [0.9 1 1.1];
    %for ii = 1:nSlices
    %    imgAll(:,:,ii) = sliceAmps(ii)*img; % + img.*abs(randn(size(img)));
    %end
    img = imgAll; clear imgAll;
    [x,y] = meshgrid(-N/2:N/2-1);
    sigma = [5 10 20 30 5];%20*ones(nSlices,1); % stdev of hot spots
    for ii = 1:nSlices
        hotSpot(:,:,ii) = double(~modelTest)*(-3*pi/4)*exp(-(x.^2 + (y-yPos(ii)).^2)./sigma(ii)^2); % 18 degree heat
        hotSpot(:,:,ii) = hotSpot(:,:,ii).*(img(:,:,ii) > 0); % to mask out temp outside volume for error calc
    end
    imgHot = img.*exp(1i*hotSpot);
    for ii = 1:nSlices
        dacc(:,1,ii) = col(fftshift(fft2(fftshift(imgHot(:,:,ii)))));
    end
    L = permute(img,[1 2 4 3])*Nx*Ny;
    if nLib > 1 % synthesize some library images at different positions, to test multibaseline
      for ii = 2:nLib
        L = cat(3,L,circshift(L,(ii-1)*round(Nx/nLib),2));
      end
    end
end

ct = -7.7871; % degrees C/radian (phase->temp conversion factor)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the 'accelerated' data and sampled k-space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
k = false(Nx,1);
k(1:inPlaneAcc:end) = true;
dacc = dacc(repmat(k,[Nx 1]),:,:);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% define the phase shifts & apply to one slice's data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
signPatternType = 'rand'; % switch between 'convCAIPI', 'rand' and 'chaotic'
switch signPatternType
    case 'chaotic'
        % get the chaotic sign pattern to modulate data with in k-space
        x = 0.5; % initial x
        r = 3.99; % growth rate
        for ii = 1:(nSlices-1)*Nx-1
            x(ii+1) = r*x(ii)*(1-x(ii));
        end
        kWts = [(-1).^(reshape(x(:),[Nx nSlices-1]) > 0.5) ones(Nx,1)];
    case 'convCAIPI'
        % generate phase ramps that shift the slices by fov/sliceIndex
        kWts = exp(1i*2*pi/N*(-N/2:N/2-1)'*(N/nSlices)*(0:nSlices-1));
    case 'rand' %'1,-1'
        % generate phase ramps that shift the slices by fov/sliceIndex
        kWts = exp(1i*2*pi/N*(-N/2:N/2-1)'*(N/nSlices)*(0:nSlices-1));
        % randomly permute them
        load util/permutation
        %permutation = randperm(N);
        kWts = kWts(permutation,:);
        if nSlices > 1
            kWts(:,2:end) = -kWts(:,2:end);
        end
end
% duplicate to all freq enc points
kWts = kron(ones(Nx,1),kWts); % duplicate to all frequency encoded points
kWts = kWts(repmat(k,[Nx 1]),:); % remove unsampled lines
daccCaipi = dacc;%.*repmat(kWts,[1 Nc]);
for ii = 1:nSlices
    daccCaipi(:,:,ii) = bsxfun(@times,dacc(:,:,ii),kWts(:,ii));%.*repmat(kWts,[1 Nc]);
end
figure;
imagesc(abs(ft2(sum(reshape(daccCaipi,[sum(k) Ny Nc nSlices]),4))));
axis image,colormap gray,axis off
figure;
imagesc(-angle(ft2(sum(reshape(daccCaipi,[sum(k) Ny Nc nSlices]),4))),[-pi/2 pi/2]);
axis image;axis off;colorbar

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4x-accelerated k-space recon with FFT's
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
thetainit = zeros(Nx,Ny,nSlices); % initialize temp phase shift map with zeros
acqp.data = sum(daccCaipi,3) + ...
    noiseStd*N/sqrt(2)*(randn(size(sum(daccCaipi,3))) + 1i*randn(size(sum(daccCaipi,3)))); % accelerated data
acqp.k = k(:,1); % k-space sampling mask - passing a vector tells ksh to
                 % preemptively do FFT in fully-sampled dimension, and run in hybrid space
acqp.kWts = kWts; %(repmat(k,[Nx 1]),1:nSlices); % in case we don't use all the weights for testing
acqp.L = L; % baseline 'library'
acqp.L = permute(acqp.L,[3 4 1 2]); % permute to # library images, # slices, space
acqp.L = permute(acqp.L(:,:,:),[3 1 2]); % collapse spatial dimensions and permute to space, # library images, # slices
%acqp.L = acqp.L(:,:).'; % slices is second dimension
algp.order = 0; % polynomial order
algp.lam = [10^-4.5 -1]; % sparsity regularization parameter
algp.beta = 2^-16; % roughness regularization parameter
algp.useGPU = false; % cast all variables to the GPU (Cartesian Only)
algp.modeltest = modelTest; % measure temperature errors without heating
tic
[thetakacc,~,~,~,Ac] = kspace_hybrid_thermo_inccaipi(acqp,thetainit,algp);
tempkacc = ct*real(thetakacc);
toc
rmsError = norm(ct*(thetakacc(:)-hotSpot(:)))/N
maxError = max(abs(ct*(thetakacc(:)-hotSpot(:))))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Show the results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
if modelTest
    maxDisplayTemp = 1;minDisplayTemp = -1;
else
    maxDisplayTemp = max(ct*hotSpot(:));minDisplayTemp = 0;
end
for ii = 1:nSlices
    subplot(3,nSlices,ii); imagesc(ct*hotSpot(:,:,ii),[minDisplayTemp maxDisplayTemp]); axis image;axis off
    h = colorbar; ylabel(h,'degrees C');
    title(sprintf('Truth, slice %d',ii));
    subplot(3,nSlices,nSlices+ii); imagesc(tempkacc(:,:,ii),[minDisplayTemp maxDisplayTemp]); axis image;axis off
    h = colorbar; ylabel(h,'degrees C');
    title(sprintf('Incoherent CAIPI, slice %d',ii));
    subplot(3,nSlices,2*nSlices+ii); imagesc(abs(ct*hotSpot(:,:,ii)-tempkacc(:,:,ii)),[minDisplayTemp 1]); axis image;axis off
    h = colorbar; ylabel(h,'degrees C');
    title(sprintf('Error, slice %d',ii));
end
